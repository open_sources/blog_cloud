package com.blog.blog_interface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class BlogInterfaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogInterfaceApplication.class, args);
    }

}
