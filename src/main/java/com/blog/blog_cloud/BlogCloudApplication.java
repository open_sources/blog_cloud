package com.blog.blog_cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogCloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogCloudApplication.class, args);
    }

}
