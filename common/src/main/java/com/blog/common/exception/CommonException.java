package com.blog.common.exception;

import com.blog.common.enums.CommonReturnParameter;

/**
 * <p>Title: CommonException</p >
 * <p>Description: 公共异常处理类</p >
 * <p>Company: http://www.yinjiedu.com</p >
 * <p>Project: blog_cloud</p >
 *
 * @author: qiwei
 * @Date: 2019/8/4 22:24
 * @Version: 1.0
 */
public class CommonException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private Integer code;


    public CommonException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public CommonException(CommonReturnParameter commonReturnParameter) {
        super(commonReturnParameter.getMessage());
        this.code = commonReturnParameter.getCode();
    }
}
