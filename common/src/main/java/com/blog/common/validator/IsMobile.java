package com.blog.common.validator;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * <p>Title: IsMobile</p >
 * <p>Description: 手机号码校验注解定义</p >
 * <p>Company: http://www.yinjiedu.com</p >
 * <p>Project: blog_cloud</p >
 *
 * @author: qiwei
 * @Date: 2019/8/6 21:04
 * @Version: 1.0
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {IsMobileValidator.class})
public @interface IsMobile {

    boolean required() default true;

    String message() default "手机号码格式错误";

}