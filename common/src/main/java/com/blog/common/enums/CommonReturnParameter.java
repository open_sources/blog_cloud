package com.blog.common.enums;

import lombok.Getter;

/**
 * <p>Title: CommonReturnParameter</p >
 * <p>Description: 接口请求公共返回参数定义，在开中严格按照此状态码、状态说明返回</p >
 * <p>Company: http://www.yinjiedu.com</p >
 * <p>Project: blog_cloud</p >
 *
 * @author: qiwei
 * @Date: 2019/8/4 22:26
 * @Version: 1.0
 */
@Getter
public enum CommonReturnParameter {

    //通用操作成功code  2001XX
    REQUEST_SUCCESS(200100, "请求成功"),
    OPERATE_SUCCESS(200101, "操作成功"),
    UPLOAD_SUCCESS(200102, "文件上传成功"),
    UPDATE_SUCCESS(200103, "更新成功"),

    //通用的操作错误码  5001XX
    REQUEST_FAILD(500100, "请求失败"),
    OPERATE_FAILD(500101, "操作失败"),
    UPLOAD_FAILD(500102, "文件上传失败"),
    UPDATE_FAILD(500103, "更新失败"),
    SYSTEM_ERROR(5001, "系统错误"),
    PARAMETER_ERROR(500104, "参数错误"),

    //登录模块返回码 5002XX
    SESSION_ERROR(500201, "Session不存在或者已经失效"),
    PASSWORD_EMPTY(500202, "登录密码不能为空"),
    MOBILE_EMPTY (500203, "手机号不能为空"),
    MOBILE_ERROR(500204, "手机号格式错误"),
    MOBILE_NOT_EXIST(500205, "手机号不存在"),
    PASSWORD_ERROR(500206, "密码错误"),
    ACCOUNT_EMPTY(500207, "账号不能为空"),
    ACCOUTN_IS_NOT_EXIST(500208, "账号不存在"),
    ;

    private Integer code;

    private String message;

    CommonReturnParameter(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
