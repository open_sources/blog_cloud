package com.blog.common.utils;

import java.util.UUID;

/**
 * <p>Title: UUIDUtil</p >
 * <p>Description: uuid转换</p >
 * <p>Company: http://www.yinjiedu.com</p >
 * <p>Project: blog_cloud</p >
 *
 * @author: qiwei
 * @Date: 2019/8/4 22:21
 * @Version: 1.0
 */
public class UUIDUtil {
    /**
     * @description: uuid转换，将默认的"-"转换为""
     * @auther: qiwei
     * @date: 2019/7/21 22:20
     * @return:
     */
    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
