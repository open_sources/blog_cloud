package com.blog.common.utils;

import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Title: ValidatorUtil</p >
 * <p>Description: 校验工具类</p >
 * <p>Company: http://www.yinjiedu.com</p >
 * <p>Project: blog_cloud</p >
 *
 * @author: qiwei
 * @Date: 2019/8/6 20:55
 * @Version: 1.0
 */
public class ValidatorUtil {

    /**
     * 手机号码校验正则
     */
    private static final Pattern mobile_pattern = Pattern.compile("1\\d{10}");

    /**
     * @description: 手机号码校验方法
     * @auther: qiwei
     * @date: 2019/8/6 20:56
     * @param cellPhone 手机号码
     * @return: boolean
     */
    public static boolean isMobile(String cellPhone) {
        if(StringUtils.isEmpty(cellPhone)) {
            return false;
        }
        Matcher matcher = mobile_pattern.matcher(cellPhone);
        return matcher.matches();
    }
}
